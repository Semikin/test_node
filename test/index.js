const http = require("http");
const path = require("path");
const fs  = require("fs");
const mime = require('mime');
const expect = require("chai").expect;

async function sendFile(callback){
  let url = "http://localhost:8080/files/123";
  let filepath = path.resolve('./test/assets/123.jpg')
  let filedata = fs.readFileSync(filepath);
  let filetype = mime.lookup(filepath);
  let options = {
    method: 'PUT',
    headers: {
      'Content-Type': filetype,
      'Content-Length': Buffer.byteLength(filedata)
    }
  };
  let req =  http.request(url, options, callback);
  req.write(filedata);
  req.end();
  return true
}
describe("Модуль HTTP API", function() {
  after(function() { 
    console.log("Очистка папки data  и файла db/metadata.txt");
    const directory = path.resolve('./data');
    const metafile = path.resolve('./db/metadata.txt');
    fs.readdir(directory, (err, files) => {
      if (err) throw err;

      for (const file of files) {
        fs.unlink(path.join(directory, file), err => {
          if (err) throw err;
        });
      }
    });
    fs.truncate(metafile, 0, function(err) {
      if (err) throw err;
      return true
    });
  });
  describe("PUT запрос", function() {
    it("возвращает статус 200, если файл сохранен", function(done) {
      sendFile(function(res){
        expect(res.statusCode).to.equal(200);
        done();
      });
            
    });
  });
  describe("GET запрос", function() {
    it("возвращает статус 200, если файл найден", function(done) {
      sendFile().then(function(res){
        let url = "http://localhost:8080/files/123";
        let req =  http.get(url, function(response) {
          expect(response.statusCode).to.equal(200);
          done();
        });
      })
    });
    it("возвращает статус 404, если файл не найден", function(done) {
      let url = "http://localhost:8080/files/1123";
      let req =  http.get(url, function(response) {
        expect(response.statusCode).to.equal(404);
        done();
      })
    }); 
  });
});