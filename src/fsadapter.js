const fs = require("fs");
const path = require("path");

const storagePath = path.resolve("./data");
fs.access(storagePath, fs.constants.F_OK, err => {
  if (err) {
    fs.mkdir(storagePath, err => {
      if (err) {
        throw err;
      }
    });
  }
});
function writeFile(filename) {
  const filepath = path.join(storagePath, filename);
  return fs.createWriteStream(filepath);
}
function readFile(filename) {
  const filepath = path.join(storagePath, filename);
  return fs.createReadStream(filepath);
}
module.exports = {
  writeFile,
  readFile
};
