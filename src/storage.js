const fsadapter = require("./fsadapter");

function saveFile(filename) {
  return fsadapter.writeFile(filename);
}
function getFile(filename) {
  return fsadapter.readFile(filename);
}
module.exports = {
  saveFile,
  getFile
};
