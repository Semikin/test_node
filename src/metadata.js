const fs = require("fs");
const path = require("path");

const metafolder = path.resolve("./db");
const storagefolder = path.resolve("./data");
const metafile = path.join("/", metafolder, "metadata.txt");

fs.access(metafolder, fs.constants.F_OK && fs.constants.S_IFDIR, err => {
  if (err) {
    fs.mkdir(metafolder, err => {
      if (err) {
        throw err;
      }
      fs.writeFile(metafile, "", () => {
        return true;
      });
    });
  }
  fs.access(metafile, fs.constants.F_OK, err => {
    if (err) {
      fs.writeFile(metafile, "", () => {
        return true;
      });
    }
  });
});

function saveMeta(filename, type, callback) {
  fs.stat(path.join("/", storagefolder, filename), (err, stats) => {
    if (err) throw err;
    let fdata = [filename, type, stats.size];
    fs.readFile(metafile, "utf8", function(err, data) {
      if (err) throw err;
      let dataArr = data
        .toString()
        .split("\n")
        .filter(x => x !== "")
        .map(x => x.split(";"));
      const filestr = dataArr.find(x => x[0] == filename);
      if (filestr === undefined) {
        dataArr.push(fdata);
      } else {
        const strIndex = dataArr.indexOf(filestr);
        dataArr.splice(strIndex, 1, fdata);
      }
      const retArr = dataArr.map(x => x.join(";"));
      fs.writeFile(metafile, retArr.join("\n"), "utf8", (err) => callback(err));
    });
  });
}
function getMeta(filename, callback) {
  fs.readFile(metafile, "utf8", function(err, data) {
    if (err) throw err;
    let dataArr = data.toString().split("\n");
    return callback(null, dataArr.find(x => x.split(";")[0] == filename));
  });
}
module.exports = {
  saveMeta,
  getMeta
};
