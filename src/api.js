const http = require("http");
const storage = require("./storage");
const meta = require("./metadata");

function serve() {
  return http
    .createServer((req, res) => {
      if (req.url.search(/^\/files\/[a-zA-Z0-9._-]*$/) === 0) {
        let filename = req.url.split("/")[2];
        if (req.method === "GET") {
          meta.getMeta(filename, function(err, data) {
            if (data != undefined) {
              let dataArr = data.split(";");
              res.writeHead(200, {
                "Content-Type": dataArr[1],
                "Content-Length": dataArr[2],
              });
              storage.getFile(filename).pipe(res);
            } else {
              res.writeHead(404).end();
            }
          });
        } else if (req.method === "PUT") {
          let cType = req.headers["content-type"];
          // let cLength = req.headers["content-length"];
          req
            .pipe(storage.saveFile(filename))
            .on("close", () => { meta.saveMeta(filename, cType, (err) => {
              if (err) {res.writeHead(400).end();}
              res.writeHead(200).end();
              });
          });
        } else {
          res.writeHead(404).end();
        }
      } else {
        res.writeHead(404).end();
      }
    })
    .listen(8080);
}
module.exports = {
  serve
};
